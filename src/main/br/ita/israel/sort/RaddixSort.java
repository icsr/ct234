package br.ita.israel.sort;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class RaddixSort implements Sortable<List<String>> {

	public static final int DEFAULT_BASE = 10;
	private int base;
	private List<Queue<String>> queues = new LinkedList<Queue<String>>();
 
	public RaddixSort(int base) {
		this.base = base;
		for (int i = 0; i < DEFAULT_BASE; i++) {
			queues.add(new LinkedList<String>());
		}
	}

	/* (non-Javadoc)
	 * @see br.ita.israel.sort.Sortable#sort(java.util.List)
	 */
	@Override
	public void sort(List<String> list) {
		int factor = 1;
		for (int i = 0; i < list.get(0).length(); i++) {
			for (int j = 0; j < list.size(); j++) {
				queues.get((Integer.parseUnsignedInt(list.get(j), base) / factor) % DEFAULT_BASE).add(list.get(j));
			}
			list.clear();
			for (int k = 0; k < DEFAULT_BASE; k++) {
				while (!queues.get(k).isEmpty()) {
					list.add(queues.get(k).poll());
				}
			}
			factor *= DEFAULT_BASE;
		}
	}
	
	public void sort(Integer[] vector, int numberOfDigits){
		
		List<String> list = new ArrayList<>();
		
		for(int i : vector){
			list.add(toNormalizedString(i, numberOfDigits));
		}
		
		sort(list);
	}

	private String toNormalizedString(int i, int numberOfDigits) {
		
		StringBuilder str = new StringBuilder(Integer.toString(i));
		str.reverse();
		while(str.length() < numberOfDigits){
			str.append("0");
		}		
		return str.reverse().toString();
	}

}
