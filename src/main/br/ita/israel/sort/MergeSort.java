package br.ita.israel.sort;

public class MergeSort implements Sortable<Integer[]>{

	private Integer[] aux;

	public void sort(Integer[] vector) {
		aux = new Integer[vector.length];
		sort(vector, 0, vector.length - 1);
	}

	private Integer[] sort(Integer[] vector, int i, int f) {

		if (i >= f) {
			return aux;
		}

		int m = (i + f) / 2;
		sort(vector, i, m);
		sort(vector, m + 1, f);
		return merge(vector, i, m, f);
	}

	private Integer[] merge(Integer[] vector, int i, int m, int f) {

		int a = i, v1 = i, v2 = m + 1;
		
		while(v1 <= m && v2 <= f){
			if(vector[v1] < vector[v2]){
				aux[a++] = vector[v1++];
			}else{
				aux[a++] = vector[v2++];
			}
		}
		
		while(v1 <= m){
			aux[a++] = vector[v1++];
		}
		
		while(v2 <= f){
			aux[a++] = vector[v2++];
		}
		
		for(int j = i; j <= f; j++){
			vector[j] = aux[j];
		}
		
		return aux;
	}

}
