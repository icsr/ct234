package br.ita.israel.sort;

public class QuickSort implements Sortable<Integer[]> {

	protected Integer[] vector;

	protected boolean threeMedians;

	public QuickSort() {
		this(false);
	}

	public QuickSort(boolean threeMedians) {
		this.threeMedians = threeMedians;
	}

	@Override
	public void sort(Integer[] vector) {

		if (vector == null || vector.length == 0) {
			return;
		}

		this.vector = vector;

		sort(0, vector.length - 1);

	}

	protected void sort(int i, int f) {

		if (i >= f) {
			return;
		}

		int p = partition(i, f);
		sort(i, p - 1);
		sort(p + 1, f);

	}

	protected int partition(int i, int f) {

		int left = i + 1;
		int right = f;

		threeMedians(i, f);

		while (true) {

			while (left < f && vector[left] < vector[i])
				left++;

			while (right > i && vector[right] >= vector[i])
				right--;

			if (left >= right)
				break;

			switchValues(left, right);

		}

		switchValues(i, right);
		return right;

	}

	protected void threeMedians(int i, int f) {
		if (!threeMedians)
			return;

		int central = (i + f) / 2;
		Integer[] v = new Integer[3];

		v[0] = vector[i];
		v[1] = vector[central];
		v[2] = vector[f];

		for (int k = 0; k < v.length - 1; k++) {
			for (int t = 0; t < v.length - 1 - k; t++) {
				if (v[t + 1] < v[t]) {
					switchValues(v, t + 1, t);
				}
			}
		}

		int median = v[1];
		int index = i;
		if (vector[central] == median) {
			index = central;
		}

		if (vector[f] == median) {
			index = f;
		}

		if (index == i) {
			return;
		}

		switchValues(index, i);

	}

	private void switchValues(Integer[] v, int index1, int index2) {
		if (index1 == index2) {
			return;
		}

		int aux = v[index1];
		v[index1] = v[index2];
		v[index2] = aux;
	}

	private void switchValues(int index1, int index2) {
		switchValues(this.vector, index1, index2);
	}

}
