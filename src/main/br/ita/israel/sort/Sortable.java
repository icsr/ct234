package br.ita.israel.sort;

public interface Sortable<T> {

	void sort(T toSort);

}