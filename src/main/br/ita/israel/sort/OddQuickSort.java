package br.ita.israel.sort;

public class OddQuickSort extends QuickSort {
	
	@Override
	protected int partition(int i, int f) {
		
		int x = vector[f];
		
		int left = i - 1;
		int aux;
		for(int j = i; j < f; j++){
			if(vector[j] <= x){
				aux = vector[++left];
				vector[left] = vector[j];
				vector[j]=aux;
			}
		}
		
		aux = vector[left+1];
		vector[left+1] = vector[f];
		
		vector[f] = aux;
		return left+1;
		
	}

}
