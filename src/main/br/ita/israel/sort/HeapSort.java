package br.ita.israel.sort;

public class HeapSort implements Sortable<Integer[]>{

	private Integer[] vector;

	public HeapSort(Integer[] vector) {
		this.vector = vector;
	}
	
	public HeapSort() {
	}

	public void sift(int i){
		sift(i, vector.length);
	}

	public void sift(int i, int n) {
		int maior = i;
		int esq = 2 * i + 1;
		int dir = 2 * i + 2;
		if (esq < n && vector[esq] > vector[maior]) {
			maior = esq;
		}
		if (dir < n && vector[dir] > vector[maior]) {
			maior = dir;
		}

		if (maior == i)
			return;
		
		int aux = vector[i];
		vector[i] = vector[maior];
		vector[maior] = aux;
		sift(maior, n);
	}

	public void build() {
		
		for(int i = vector.length/2; i >= 0; i--){
			sift(i, vector.length);
		}

	}

	public void sort(Integer[] vector) {
		
		if(this.vector == null){
			this.vector = vector;
		}
		
		build();
		
		for(int i = vector.length - 1; i >= 0; i--){
			int aux = vector[i];
			vector[i] = vector[0];
			vector[0] = aux;
			sift(0, i);
		}

	}

}
