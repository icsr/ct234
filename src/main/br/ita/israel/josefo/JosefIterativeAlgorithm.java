package br.ita.israel.josefo;

public class JosefIterativeAlgorithm extends JosefAlgorithm{

	public JosefIterativeAlgorithm(double numberOfParticipants) {
		super(numberOfParticipants);
	}

	@Override
	public void start(int startPoint) {
		
		while(aliveParticipants.size() > MINIMUM_SIZE){
			int nextParticipant = (startPoint + 1) % aliveParticipants.size();
			kill(nextParticipant);			
			startPoint = nextParticipant;
		}
		
		
	}

}
