package br.ita.israel.josefo;

import java.util.LinkedList;

public abstract class JosefAlgorithm {

	LinkedList<Participant> aliveParticipants;
	LinkedList<Participant> killedParticipants;
	
	static final int MINIMUM_SIZE = 1;

	public JosefAlgorithm(double numberOfParticipants) {
		aliveParticipants = new LinkedList<Participant>();
		killedParticipants = new LinkedList<Participant>();

		for (int i = 0; i < numberOfParticipants; i++) {
			aliveParticipants.add(new Participant());
		}

		Participant.counter = 0;
	}

	public abstract void start(int startPoint);

	void kill(int nextParticipant) {
		killedParticipants.addLast(aliveParticipants.remove(nextParticipant));
		killedParticipants.peekLast().kill();
	}
	
	public int getJosef(){
		return aliveParticipants.peek().getPosition();
		
	}
	
	public int getLastKilled(){
		return killedParticipants.getLast().getPosition();
	}
	
	
}