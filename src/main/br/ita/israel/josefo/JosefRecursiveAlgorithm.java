package br.ita.israel.josefo;

public class JosefRecursiveAlgorithm extends JosefAlgorithm {

	public JosefRecursiveAlgorithm(double numberOfParticipants) {
		super(numberOfParticipants);
	}

	@Override
	public void start(int startPosition) {
		int nextParticipant = (startPosition + 1) % aliveParticipants.size();
		
		if (aliveParticipants.size() <= MINIMUM_SIZE) {
			return;
		}

		kill(nextParticipant);
		start(nextParticipant);

	}

}
