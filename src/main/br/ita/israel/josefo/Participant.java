package br.ita.israel.josefo;

public class Participant {
	static int counter = 0;
	private int position;
	private boolean alive;

	public Participant() {
		position = counter++;
		alive = true;
	}

	public int getPosition() {
		return this.position;
	}

	public void kill() {
		alive = false;
	}

	public boolean isAlive() {
		return alive;
	}
}