package br.ita.israel.queues;

import br.ita.israel.sort.HeapSort;

public class PriorityQueues {

	final Integer[] vector;
	int size;
	HeapSort hs;

	public PriorityQueues(int maxSize) {
		vector = new Integer[maxSize];
		size = 0;
		hs = new HeapSort(vector);
	}

	int max() {
		if (size == 0) {
			throw new ArrayIndexOutOfBoundsException("Fila vazia!");
		}

		return vector[0];

	}

	public int extractMax(){
		if (size == 0) {
			throw new ArrayIndexOutOfBoundsException("Fila vazia!");
		}

		int max = vector[0];

		vector[0] = vector[size - 1];
		hs.sift(0, --size);

		return max;
	}

	public void modify(int index, int value) {
		if (index < 0 || index >= size) {
			throw new ArrayIndexOutOfBoundsException(index);
		}

		vector[index] = value;
		hs.build();

	}

	public void insert(int... values) {
		for (int v : values) {
			if(size == vector.length){
				throw new ArrayIndexOutOfBoundsException("Sem espaço na fila");
			}
			modify(size++, v);
		}
	}

	public void print() {
		System.out.println("Exibindo a fila de prioridades:");
		for (int i = 0; i < size; i++) {
			System.out.print(String.format("%d ", vector[i]));
		}
		System.out.println(String.format("\nMáximo: %d", max()));
	}

}
