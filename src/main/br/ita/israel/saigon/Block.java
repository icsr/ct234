package br.ita.israel.saigon;

public class Block {
	
	private int size;

	public Block(int size) {
		this.size = size;
	}

	@Override
	public String toString() {
		return "Bloco " + size;
	}

	public boolean isHavierThan(Block item) {
		return this.size > item.size;
	}

	public int getSize() {
		return size;
	}
		

}