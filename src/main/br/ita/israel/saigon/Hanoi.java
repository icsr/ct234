package br.ita.israel.saigon;

public class Hanoi extends Saigon{
	
	public Hanoi(int numberOfBlocks) {
		super(numberOfBlocks, 1);
		
	}

	public Hanoi(int numberOfBlocks, boolean printSteps) {
		this(numberOfBlocks);
		this.printSteps = printSteps;
	}
	
	

}
