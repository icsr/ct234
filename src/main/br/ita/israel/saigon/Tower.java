package br.ita.israel.saigon;

import java.util.Iterator;
import java.util.Stack;

public class Tower extends Stack<Block> {

	private static final long serialVersionUID = 8219617371542546511L;
	protected String name;

	public Tower(String name) {
		this.name = name;
	}

	public String toString() {

		StringBuilder str = new StringBuilder();

		str.append("\n\n> Torre ").append(this.name).append(" está com os blocos:");

		Iterator<Block> it = this.iterator();

		while (it.hasNext()) {
			str.append("\n").append(it.next());
		}

		return str.toString();

	}

	@Override
	public Block push(Block item) {

		if (!this.empty() && item.isHavierThan(this.peek())) {
			throw new Error("Empilhado Bloco " + item.getSize() + " em cima de outro "+ this.peek().getSize() +" mais leve.");
		}

		return super.push(item);
	}

}
