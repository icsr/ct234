package br.ita.israel.saigon;

import java.util.Stack;

public class HanoiIteractive {

	private Stack<Integer> stack = new Stack<>();

	public void perform(int numberOfBlocks, int origin, int destination, int auxiliary) {
		step = 0;

		stack.push(auxiliary);
		stack.push(destination);
		stack.push(origin);
		stack.push(numberOfBlocks);

		while (!stack.isEmpty()) {
			Integer n = stack.pop();
			Integer o = stack.pop();
			Integer d = stack.pop();
			Integer a = stack.pop();

			if (n == 1) {
				move(o, d);
				if(!stack.isEmpty()){
					move(stack.pop(), stack.pop());
				}
				continue;
			}
		
			stack.push(o);
			stack.push(d);
			stack.push(a);
			stack.push(n-1);
			
			stack.push(d);
			stack.push(o);
			
			stack.push(d);
			stack.push(a);
			stack.push(o);
			stack.push(n-1);
			
		}

	}

	int step = 0;
	private void move(int origin, int destination) {
		System.out.println(String.format("Passo %5d: mova de %d para %d", ++step, origin, destination));
	}
	
	public static void main(String[] args) {
		new HanoiIteractive().perform(10, 1, 2, 3);
	}

}