package br.ita.israel.saigon;

import java.util.Stack;

public class Saigon{

	protected Tower originTower;
	protected Tower destinyTower;
	protected Tower[] auxiliariesTower;
	private int iteration = -1;
	boolean printSteps = false;

	public Saigon(int numberOfBlocks, int numberOfAuxiliariesTowers) {
		originTower = new Tower("Origem");
		destinyTower = new Tower("Destino");
		auxiliariesTower = new Tower[numberOfAuxiliariesTowers];
		
		
		for(int i = 0; i < numberOfAuxiliariesTowers; i++){
			auxiliariesTower[i] = new Tower("Auxiliar " + (i+1));
		}
		

		for (int i = 0; i < numberOfBlocks; i++) {
			originTower.push(new Block(numberOfBlocks - i));
		}

		status();

	}

	public Saigon(int numberOfBlocks, int numberOfAuxiliariesTowers, boolean printSteps) {
		this(numberOfBlocks, numberOfAuxiliariesTowers);
		this.printSteps = printSteps;
	}

	public int start() {
		saigon(originTower.size(), originTower, destinyTower, auxiliariesTower);
		
		int remainingBlocks = originTower.size();
		for(Tower aux : auxiliariesTower){
			remainingBlocks += aux.size();
		}		
		
		if(remainingBlocks != 0){
			throw new Error("Faltou mover " + remainingBlocks + " para o destino.");
		}
		
		return iteration;
	}

	private void status() {
		
		++iteration;
		
		if(!printSteps ){
			return;
		}


		System.out.println("\n\n>> Iteração " + iteration);
		System.out.println(originTower);
		System.out.println(destinyTower);

		for(Tower t : auxiliariesTower){
			System.out.println(t);
		}
		
	}
	
	protected void saigon(int numberOfBlocks, Tower origin, Tower destination, Tower... auxiliaries) {
		
		if (numberOfBlocks == 1) {
			move(origin, destination);
			return;
		}
		
		if(numberOfBlocks == 0){
			return;
		}
				
		int partitionSize = (numberOfBlocks - 1)/auxiliaries.length;
		int topSize = (numberOfBlocks - 1) % auxiliaries.length;
		
		for(int i = 0; i < auxiliaries.length; i++){
			saigon(correctTopSizeWhenNumberOfBlocksPerAuxiliaryTowerIsOdd(partitionSize, topSize, i), origin, auxiliaries[i], destination);
		}
		
		move(origin, destination);
		
		for(int i = auxiliaries.length - 1; i >= 0; i--){
			saigon(correctTopSizeWhenNumberOfBlocksPerAuxiliaryTowerIsOdd(partitionSize, topSize, i), auxiliaries[i], destination, origin);
		}
		
	}

	private int correctTopSizeWhenNumberOfBlocksPerAuxiliaryTowerIsOdd(int partitionSize, int topSize, int i) {
		return i == 0 && topSize != 0 ? topSize + partitionSize : partitionSize;
	}


	public void move(Stack<Block> origin, Stack<Block> destination) {
		destination.push(origin.pop());
		status();
	}

	boolean underTest = false;


}
	