package br.ita.israel.greycode;

import java.util.ArrayList;
import java.util.List;

public class GreyCode {

	private int[][] codes;
	private ArrayList<String> list;

	public GreyCode start(int n) {
		
		if(n <= 0){
			return this;
		}
		
		int lines = (int) Math.pow(2, n);
		int columns = n;

		if (codes == null) {
			codes = new int[lines][columns];
		}
		
		int half = lines/2;
		for(int i = 0; i < half; i++){
			codes[i][columns-1]=0;
			codes[half+i][columns-1]=1;
		}
		
		start(n-1);
		
		for(int i = half; i < lines; i++){
			for(int j = 0; j < columns-1; j++){
				codes[i][j]=codes[lines - 1 - i][j];
			}
		}
		
		return this;

	}
	
	public List<String> getCodes(){
		
		if(list != null){
			return list;
		}
		
		list = new ArrayList<String>();
		for(int i = 0; i < codes.length; i++){
			StringBuilder str = new StringBuilder();
			for(int j = codes[0].length - 1; j >= 0; j--){
				str.append(codes[i][j]);
			}
			list.add(str.toString());
		}
		return list;
	}
	
	public void printCodes(){
		List<String> codes = getCodes();
		
		for(String s : codes){
			System.out.println(s);
		}
	}

}
