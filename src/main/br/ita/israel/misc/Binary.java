package br.ita.israel.misc;

import java.util.Stack;

public class Binary {
	
	Stack<Integer> stack = new Stack<Integer>();
	
	public void print(int number){

		int q = number;
		do{
			int r = q % 2;
			stack.push(r);
			q = q / 2;
		}while(q != 0);
		
		while(!stack.isEmpty()){
			System.out.print(String.format("%d ", stack.pop()));
		}
	}
	
	public static void printRecursively(int number){
		
		int q = number/2;
		int r = number % 2;
		
		if(r == 0){
			return;
		}
		printRecursively(q);
		System.out.print(r);
		
	}
	

}
