package br.ita.israel.misc;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class Palindromo {

	public static boolean isPalindromo(String expression) {

		expression = expression.toLowerCase();
		List<Character> list = new ArrayList<Character>();

		for (char c : expression.toCharArray()) {
			c = retiraAcentosECedilhaETremas(c);
			if(Pattern.matches("[a-z]", ""+c)){
				list.add(c);
			}

		}

		return isPalindromo(list, 0, list.size() - 1);
	}

	private static boolean isPalindromo(List<Character> list, int i, int f) {
		
		if(i >= f) return true;
		
		return list.get(i).equals(list.get(f)) && isPalindromo(list, i+1, f-1);
	}

	private static char retiraAcentosECedilhaETremas(char c) {

		if (c == 'á' || c == 'ã' || c == 'â' || c == 'à') {
			return 'a';
		} else if (c == 'é' || c == 'ê' || c == 'ë') {
			return 'e';
		} else if (c == 'í' || c == 'ĩ' || c == 'î' || c == 'ï') {
			return 'i';
		} else if (c == 'ó' || c == 'ô') {
			return 'o';
		} else if (c == 'ç') {
			return 'c';
		} else {
			return c;
		}
	}

}
