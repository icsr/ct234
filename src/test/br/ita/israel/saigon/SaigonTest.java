package br.ita.israel.saigon;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.junit.Test;

import br.ita.israel.tools.Timer;

public class SaigonTest {

	@Test
	public void saigon2withSteps() {

		Saigon saigon = new Saigon(8, 2, true);

		Timer.tic();
		saigon.start();
		Timer.toc();

	}

	@Test
	public void saigonVSHanoi() {

		execute(1, 2, 1, 1, 3, 5, 8, 10, 15, 20, 25, 26);

	}

	@Test
	public void saigon2VSSaigon3() {

		execute(2, 3, 1, 3, 4, 5, 8, 10, 15, 20, 25);

	}

	private void execute(int numberOfAuxTowers1, int numberOfAuxTowers2, int... numbersOfBlocks) {

		List<Score> score1 = new ArrayList<Score>();
		List<Score> score2 = new ArrayList<Score>();

		for (int i : numbersOfBlocks) {
			execute(i, score1, new Saigon(i, numberOfAuxTowers1));
			execute(i, score2, new Saigon(i, numberOfAuxTowers2));
		}

		IntStream.range(0, score2.size()).forEach(i -> {
			System.out.printf("%3d blocos: S%d/S%d: %10d/%10d iterações em %5d/%5d ms\n",
					score2.get(i).getNumberOfBlocks(), numberOfAuxTowers1, numberOfAuxTowers2,
					score1.get(i).getIterationCounts(), score2.get(i).getIterationCounts(),
					score1.get(i).getExecutionTimeInMils(), score2.get(i).getExecutionTimeInMils());
		});
	}

	private void execute(int numberOfBlocks, List<Score> scores, Saigon saigon) {
		System.out.printf("\n\nExecutando com %d blocos e %d torres de auxílio...\n", numberOfBlocks, saigon.auxiliariesTower.length);
		Timer.tic();
		int iterations;
		iterations = saigon.start();
		int toc = Timer.toc();
		scores.add(new Score(numberOfBlocks, toc, iterations));
	}

}

class Score {
	private final int executionTimeInMils;
	private final int iterationCounts;
	private final int numberOfBlocks;

	public Score(int numberOfBlocks, int executionTimeInMils, int iterationCounts) {
		this.executionTimeInMils = executionTimeInMils;
		this.iterationCounts = iterationCounts;
		this.numberOfBlocks = numberOfBlocks;
	}

	public int getNumberOfBlocks() {
		return numberOfBlocks;
	}

	public int getExecutionTimeInMils() {
		return executionTimeInMils;
	}

	public int getIterationCounts() {
		return iterationCounts;
	}

}
