package br.ita.israel.tools;

import java.util.Date;

public class Timer {

	static Date startTime;

	public static void tic() {
		startTime = new Date();
	}

	public static int toc() {
		return (int) (new Date().getTime() - startTime.getTime());
	}

}