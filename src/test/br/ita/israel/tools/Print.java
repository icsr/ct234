package br.ita.israel.tools;

import java.util.List;

public class Print {
	
	public static void onScreen(String label, List<String> list) {
		System.out.println(label);
		for(String s : list){
			System.out.println(s);
		}
		System.out.println("");
	}
	
	public static void onScreen(String label, Integer[] vector) {
		System.out.print(label);
		System.out.print(" ");
		
		if(vector.length > 50) return;
		
		for(int i : vector){
			System.out.print(String.format("%d\t", i));
		}
		System.out.println("");
	}

}
