package br.ita.israel.queues;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class PriorityQueuesTest {

	private PriorityQueues pq;

	@Before
	public void setUp() throws Exception {
		pq = new PriorityQueues(1000);
		pq.insert(new int[] { 8, 7, 10, 1, 5, 6, 9, 100, 0 });
	}
	
	@Test
	public void testMax() throws Exception {
		assertEquals(100, pq.max());
	}

	@Test
	public void testExtractMax() {
		pq.print();
		assertEquals(100, pq.extractMax());
		assertEquals(10, pq.extractMax());
		assertEquals(9, pq.extractMax());
		assertEquals(8, pq.extractMax());
		assertEquals(7, pq.extractMax());
		assertEquals(6, pq.extractMax());
		assertEquals(5, pq.extractMax());
		assertEquals(1, pq.extractMax());
		assertEquals(0, pq.extractMax());
	}

	@Test
	public void testInsert() {
		pq.insert(1000);
		pq.insert(200);
		
		pq.print();
		
		assertEquals(1000, pq.extractMax());
		assertEquals(200, pq.extractMax());
		assertEquals(100, pq.max());
	}
	
	@Test
	public void testModify(){
		pq.modify(2, 1000);
		pq.modify(5, 200);
		pq.print();
		assertEquals(1000, pq.extractMax());
		assertEquals(200, pq.extractMax());
	}
	
	@Test
	public void testModifyLastLeaves(){
		pq.modify(pq.size - 1, 1000);
		pq.modify(pq.size - 2, 200);
		pq.print();
		assertEquals(1000, pq.extractMax());
		assertEquals(200, pq.extractMax());
	}

}
