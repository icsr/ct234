package br.ita.israel.greycode;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.ita.israel.tools.Timer;

public class GreyCodeTest {

	private GreyCode gc;

	@Before
	public void init() {
		gc = new GreyCode();
	}

	@Test
	public void threeFirstsTest() {

		List<String> codesList = performTest(3);

		assertTrue(codesList.size() == 8);
		assertEquals(codesList.get(0), "000");
		assertEquals(codesList.get(1), "001");
		assertEquals(codesList.get(2), "011");
		assertEquals(codesList.get(3), "010");
		assertEquals(codesList.get(4), "110");
		assertEquals(codesList.get(5), "111");
		assertEquals(codesList.get(6), "101");
		assertEquals(codesList.get(7), "100");

	}

	@Test
	public void twoFirstsTest() {

		List<String> codesList = performTest(2);

		assertTrue(codesList.size() == 4);
		assertEquals(codesList.get(0), "00");
		assertEquals(codesList.get(1), "01");
		assertEquals(codesList.get(2), "11");
		assertEquals(codesList.get(3), "10");

	}

	@Test
	public void fourFirstsTest() {

		List<String> codesList = performTest(4);

		assertTrue(codesList.size() == 16);
		assertEquals(codesList.get(0), "0000");
		assertEquals(codesList.get(1), "0001");
		assertEquals(codesList.get(2), "0011");
		assertEquals(codesList.get(3), "0010");
		assertEquals(codesList.get(4), "0110");
		assertEquals(codesList.get(5), "0111");
		assertEquals(codesList.get(6), "0101");
		assertEquals(codesList.get(7), "0100");

		assertEquals(codesList.get(8), "1100");
		assertEquals(codesList.get(9), "1101");
		assertEquals(codesList.get(10), "1111");
		assertEquals(codesList.get(11), "1110");
		assertEquals(codesList.get(12), "1010");
		assertEquals(codesList.get(13), "1011");
		assertEquals(codesList.get(14), "1001");
		assertEquals(codesList.get(15), "1000");

		assertEquals(Math.pow(2, 4), codesList.size(), 0);

	}

	@Test
	public void twelveFirstsTest() {
		performTest(20);
	}

	@Test
	public void JustOneTest() {
		performTest(1);
	}
	
	@Test
	public void tenFirstsTest() {
		performTest(10);
	}

	private List<String> performTest(int n) {
		List<String> codesList = execute(n);

		assertEquals(Math.pow(2, n), codesList.size(), 0);
		
		return codesList;
	}

	private List<String> execute(int n) {
		Timer.tic();
		gc.start(n);
		int time = Timer.toc();

		List<String> codesList = gc.getCodes();
		System.out.println(String.format("\n>> Tempo de execução para %d bits foi %d milissegundos.", n, time));

		if (n <= 10) {
			System.out.println(">> Imprimindo a lista...");
			gc.printCodes();
		}

		return codesList;
	}

}
