package br.ita.israel.sort;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.ita.israel.tools.Print;

public class RaddixSortTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		
		RaddixSort raddix = new RaddixSort(2);
		
		List<String> list = new ArrayList<String>();
		list.add("0110");
		list.add("1011");
		list.add("1100");
		list.add("1111");
		list.add("1000");
		list.add("0000");
		list.add("0011");
		
		Print.onScreen("Antes", list);
		
		raddix.sort(list);
		
		assertEquals("0000", list.get(0));
		assertEquals("0011", list.get(1));
		assertEquals("0110", list.get(2));
		assertEquals("1000", list.get(3));
		assertEquals("1011", list.get(4));
		assertEquals("1100", list.get(5));
		assertEquals("1111", list.get(6));
		
		Print.onScreen("Depois", list);
	}

}
