package br.ita.israel.sort;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import br.ita.israel.tools.Print;

public class HeapSortTest {
	
	Integer[] vector;

	@Before
	public void setUp() throws Exception {
		
		vector = new Integer[]{ 8, 7, 10, 1, 5 , 6, 9, 100, 0 };
	}

	@Test
	public void test() {
		
		Print.onScreen("Antes:", vector);
		HeapSort hs = new HeapSort();
		hs.sort(vector);
		Print.onScreen("Depois:", vector);
		
		assertEquals(0, (int)vector[0]);
		assertEquals(1, (int)vector[1]);
		assertEquals(5, (int)vector[2]);
		assertEquals(6, (int)vector[3]);
		assertEquals(7, (int)vector[4]);
		assertEquals(8, (int)vector[5]);
		assertEquals(9, (int)vector[6]);
		assertEquals(10, (int)vector[7]);
		assertEquals(100, (int)vector[8]);
		
	}

}

