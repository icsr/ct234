package br.ita.israel.sort;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import br.ita.israel.tools.Timer;

public class SortComparison {
	
	private Integer[] vector;
	private static Map<String, Integer> map = new HashMap<String, Integer>();;
	
	@AfterClass
	public static void printRank() {
		Iterator<String> it = map.keySet().iterator();
		
		while(it.hasNext()){
			String s = it.next();
			System.out.println(s + " - " + map.get(s));
		}
	}

	@Before
	public void setUp() {
		
		vector = new Integer[1000000];
		for(int i = 0; i < vector.length; i++){
			vector[i] = (int)(Math.random()*100000);
		}
		
	}

	@Test
	public void testMerge() {
		
		Timer.tic();
		MergeSort m = new MergeSort();
		m.sort(vector);
		map.put("ms", Timer.toc());
		
	}
	
	@Test
	public void testOddQuick() {
		
		Timer.tic();
		QuickSort m = new OddQuickSort();
		m.sort(vector);
		map.put("oq", Timer.toc());
		
	}
	

	@Test
	public void testQuick() {
		
		Timer.tic();
		QuickSort m = new QuickSort();
		m.sort(vector);
		map.put("qs", Timer.toc());
		
	}
	
	@Test
	public void testQuickWithThreeMedians() {
		
		Timer.tic();
		QuickSort m = new QuickSort(true);
		m.sort(vector);
		map.put("q3", Timer.toc());
		
	}
	
	@Test
	public void testRaddix() {
		
		Timer.tic();
		RaddixSort m = new RaddixSort(10);
		m.sort(vector, 3);
		map.put("rs", Timer.toc());
		
	}
	
	@Test
	public void testHeap() {
		
		Timer.tic();
		HeapSort m = new HeapSort();
		m.sort(vector);
		map.put("hs", Timer.toc());		
	}
	
	

}
