package br.ita.israel.sort;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import br.ita.israel.tools.Print;

public class MergeSortTest {
	
	public Integer[] vector;
	
	@Before
	public void tearUp() {
		vector = new Integer[]{15,21,0,9,7,10,11, 14,35,36,44,100};
		
		Print.onScreen("Antes:", vector);
	}

	@Test
	public void testSort() {
		
		new MergeSort().sort(vector);
		Print.onScreen("Depois:", vector);
		
		assertEquals(0, (int)vector[0]);
		assertEquals(7, (int)vector[1]);
		assertEquals(9, (int)vector[2]);
		assertEquals(10, (int)vector[3]);
		assertEquals(11, (int)vector[4]);
		assertEquals(14, (int)vector[5]);
		assertEquals(15, (int)vector[6]);
		assertEquals(21, (int)vector[7]);
		assertEquals(35, (int)vector[8]);
		assertEquals(36, (int)vector[9]);
		assertEquals(44, (int)vector[10]);
		assertEquals(100, (int)vector[11]);
		
		
	}

}
