package br.ita.israel.sort;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import br.ita.israel.tools.Print;


public class QuicksortTest {
	
	private Integer[] vector;
	private Integer[] sortedVector;

	@Before
	public void setUp(){
		this.vector = new Integer[]{4,3,6,9,2,4,3,1,2,1,4,9,3,5,6,7,10,200,2,1,0,100,50,1,3,5,6,5,33,36,43,52,6,200,100};
		this.sortedVector = new Integer[]{0,1,1,1,1,2,2,2,3,3,3,3,4,4,4,5,5,5,6,6,6,6,7,9,9,10,33,36,43,50,52,100,100,200,200};
	}

	@Test
	public void testSortWithoutThreeMedians() {
		System.out.println("\nSem três medianas");
		QuickSort q = new QuickSort();
		q.sort(vector);
		
		Print.onScreen("Fim", this.vector);
		
		check();
		
	}
	
	@Test
	public void testSortWithThreeMedians() {
		System.out.println("\nCom três medianas");
		
		QuickSort q = new QuickSort(true);
		q.sort(vector);
		
		
		check();
		
	}

	private void check() {
		for(int i = 0; i < vector.length; i++){
			assertEquals(sortedVector[i], vector[i]);
		}
	}
	
	@Test
	public void testFoolSortWithoutThreeMedians() {
		System.out.println("\nSem três medianas");
		
		QuickSort q = new OddQuickSort();
		q.sort(new Integer[]{7,5,9});
		
		Print.onScreen("", vector);
		
		check();
		
	}

}
