package br.ita.israel.josefo;

import static org.junit.Assert.assertEquals;

import org.junit.Ignore;
import org.junit.Test;

import br.ita.israel.tools.Timer;

public class JosefAlgorithmTest {

	@Test
	public void testWithOneParticipant() {

		JosefRecursiveAlgorithm josefo = new JosefRecursiveAlgorithm(1);
		josefo.start(0);

		assertEquals(1, josefo.aliveParticipants.size());
		assertEquals(0, josefo.killedParticipants.size());

	}

	@Test
	public void testWithTwoParticipants() {

		startTest(2);

	}

	@Test
	public void testWithThreeParticipants() {

		assertEquals(2, startTest(3));

	}

	@Test
	public void testWithFourParticipants() {

		startTest(4);

	}

	@Test
	public void testWithFiveParticipants() {

		assertEquals(2, startTest(5));

	}

	@Test
	public void testWithSixParticipants() {

		assertEquals(4, startTest(6));

	}

	@Test
	public void testWithTenParticipants() {

		assertEquals(4, startTest(10));

	}

	@Test
	public void testWithTwelveParticipants() {

		assertEquals(8, startTest(12));

	}

	@Test
	public void testWithTwentyParticipants() {

		assertEquals(8, startTest(20));

	}

	@Test
	public void testWithFortyParticipants() {

		assertEquals(16, startTest(40));

	}
	
	@Test @Ignore
	public void testWith2Power42Participants() {

		assertEquals(16, startTest(Math.pow(2, 42)));

	}

	int startTest(double numberOPeople) {
		Timer.tic();
		System.out.println(String.format("Algoritmo recursivo com %d participantes começando da posição ZERO:", (int)numberOPeople));
		int f1 = finishTest(numberOPeople, new JosefRecursiveAlgorithm(numberOPeople));
		System.out.println(String.format("Executou em %d ms.", Timer.toc()));

		System.out.println(String.format("Algoritmo iterativo com %d participantes começando da posição ZERO:", (int)numberOPeople));
		int f2 = finishTest(numberOPeople, new JosefIterativeAlgorithm(numberOPeople));
		System.out.println(String.format("Executou em %d ms.\n", Timer.toc()));
		
		assertEquals(f1, f2);
		return f2;
	}

	private int finishTest(double numberOPeople, JosefAlgorithm josef) {
		josef.start(0);

		josef.aliveParticipants.size();
		josef.killedParticipants.size();

		StringBuilder str = new StringBuilder();
		str.append(String.format(">>> %d era a posição de Josefo!",
				josef.getJosef()));
		str.append(String.format("\n>>> %d foi o último a morrer!",
				josef.getLastKilled()));
		str.append("\n");
		

		System.out.println(str.toString());
		
		return josef.getJosef();
	}

}

