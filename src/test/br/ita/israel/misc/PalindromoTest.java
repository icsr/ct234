package br.ita.israel.misc;

import static org.junit.Assert.*;

import org.junit.Test;

public class PalindromoTest {

	@Test
	public void test() {
		assertTrue(Palindromo.isPalindromo("Socorram-me, subi-no ônibus em Marrocos."));		
		
		assertTrue(Palindromo.isPalindromo("Anotaram a data da maratona!"));
		
		assertTrue(Palindromo.isPalindromo("O pó de cocaína mata maníaco cedo, pô!"));
		
		assertTrue(Palindromo.isPalindromo("A torre da derrota."));
		
		assertTrue(Palindromo.isPalindromo("A cara rajada da jararaca."));
		
		assertTrue(Palindromo.isPalindromo("O galo ama o lago."));
		
		assertTrue(Palindromo.isPalindromo("O lobo ama o bolo."));
		
		assertTrue(Palindromo.isPalindromo("O romano acata amores a damas amadas e Roma ataca o namoro!"));
		
		assertFalse(Palindromo.isPalindromo("Socorre-me, subi-no ônibus em Marrocos."));
	}

}
